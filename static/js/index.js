/*------------------------------------------------------------------
 Copyright (c) 2013-2014 Viktor Bezdek
 - Released under The MIT License.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 ----------------------------------------------------------------*/

$(function() {
    var config = window.ghostentista.config;
    var $window = $(window);
    var $actualPost = ($('article').length === 1 && $('article').attr('id') ? $('article').attr('id') : '').replace('post', '');
    var $logo = $('#site-head-content');
    var $header = $('#site-head');

    //---------------------------------------------------------------------
    // Config Stuff
    //---------------------------------------------------------------------

    if (config.logoBackground != '') $logo.css({
        background: config.logoBackground
    });

    // ios < 7 fixed position bug
    var ios = iOSversion();
    if (ios && ios[0] <= 6) $('body').addClass('no-fixed-elements')

    //Remove extra microformat
    $('span[itemprop="org"]').closest('strong').remove();

    // logo position
    $window.scroll(function() {
        var logoHeight = $logo.height() + 40;
        var headerHeight = $header.height() - $window.scrollTop();

        // if we need to position logo
        if (headerHeight > logoHeight) {
            var marginTop = (headerHeight / 2 - logoHeight / 2) + 'px';
            $logo.parent().css({
                paddingTop: marginTop
            });
        }

        // if header is completely gone
        var $secondaryTitle = $('#secondaryTitle');
        $secondaryTitle.css({
            background: config.logoBackground
        });
        if (headerHeight <= 0) {
            if (!$secondaryTitle.hasClass('displayed')) {
                $secondaryTitle.addClass('displayed');
                $secondaryTitle.animate({
                    top: '0px'
                }, 500);
            }
        } else {
            if ($secondaryTitle.hasClass('displayed')) {
                $secondaryTitle.removeClass('displayed');
                $secondaryTitle.animate({
                    top: '-200px'
                }, 500);
            }
        }

    });

    // scroll to top button
    $('#scroll-to-top').click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 200);
    });

    // resize does equalization of post titles
    $window.resize(function() {
        $window.trigger('scroll');
    });

    setTimeout(scrollToContent, 200);

    // updates layout after init
    $window.trigger('scroll');
    $window.trigger('resize');
    setTimeout(function() {
        $('h2.post-title, h1.post-title').slabText({
            minCharsPerLine: 12
        });
        $('article.loading').each(function() {
            var $this = $(this);
            setTimeout(function() {
                $this.removeClass('loading');
                $window.trigger('resize');
            }, Math.random() * 200);
        });
    }, 200);

    // scrolls down to start of content if marker is available
    function scrollToContent() {
        var article_detail = $("article.detail-post");
        if (article_detail.length > 0) {
            $('html,body').animate({
                scrollTop: article_detail.offset().top
            }, 'slow');
        } else {
            $('html,body').animate({
                scrollTop: 120
            }, 'slow');
        }
    }

    // ios version detection helper (for annoying fixed pos bug in iOS < 7)
    // source: http://bit.ly/1c7F26O
    function iOSversion() {
        if (/iP(hone|od|ad)/.test(navigator.platform)) {
            // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
            var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
            return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
        }
    }
});

function translateES(when) {
    let singular = {'i':'un minuto', 'h':'una hora', 'd':'un día', 'w':'una semana', 'm':'un mes', 'y':'un año'};
    let plural = {'i':'minutos', 'h':'horas', 'd':'días', 'w':'semanas', 'm':'meses', 'y':'años'};

    if (when === 'c') {
        return 'ahora mismo'
    } else if (when.length === 2 && when.charAt(0) === '1') {
        return 'hace ' + singular[when.charAt(1)];
    } else {
        return 'hace ' + when.slice(0,-1) + ' ' + plural[when.slice(-1)];
    }
}

function humanDate(when) {
    Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };
    let seconds = 0;
    let result = null;

    if (typeof when === 'string' || when instanceof String) {
        when = new Date(when);
    }
    if (when instanceof Date && !isNaN(when)) {
        seconds = (new Date()).getUnixTime() - when.getUnixTime();
    }

    if (seconds >= 0) {
        if (seconds < 2*60) {
            result = 'c';
        } else if (seconds < 60*60) {
            result = (seconds/60|0) + 'i';
        } else if (seconds < 24*60*60) {
            result = (seconds/(60*60)|0) + 'h';
        } else if (seconds < 30*24*60*60) {
            result = (seconds/(24*60*60)|0) + 'd';
        } else if (seconds < 12*30*24*60*60) {
            result = (seconds/(30*24*60*60)|0) + 'm';
        } else {
            result = (seconds/(12*30*24*60*60)|0) + 'y';
        }

        result = translateES(result);
    }

    return result;
}

(function() {
    dateTimes = document.querySelectorAll('time[datetime]');
    dateTimes.forEach(function(element, index) {
        pinta = humanDate(element.getAttribute('datetime'));
        if (pinta) {
            element.textContent = pinta;
        }
    });
})();
