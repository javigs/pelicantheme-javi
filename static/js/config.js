// Ghostentista Theme
window.ghostentista = {};
var config = {
	showThemeBadge: true,				// Show or hide theme and platform credits
	showAuthorOnPostDetail: true,		// Show author bio on post detail
	googleAnalytics: '', 				// ex: UA-XXXXX-XX, if empty will not track anything
	logoBackground: 'rgba(0,0,0,0.75)',	// Enter anything which suits css background shorthand. Ex: #ffcc00, green
	appendContent: null,				// HTML or text to be appended just before closing body tag
};
window.ghostentista.config = config;
