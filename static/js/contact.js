(function() {
    var jaccept = document.getElementById('jaccept');

    if (jaccept === null) {
        return;
    }

    var jadroid = function() {
        if (jaccept.checked) {
            document.getElementById('jsendit').style.display = 'none';
            document.getElementById('jmessage').value += ' 🤖 🦾';
        } else {
            document.getElementById('jsendit').style.display = 'block';
        }
    }
    jaccept.checked = false;
    jaccept.addEventListener('click', jadroid, false);

    // Contact Form
    if ($('#jcontact_form').length) {
        $('#jcontact_form input, #jcontact_form textarea').change(function() {
            $('#jcontact_errors').html('');
            $(this).removeClass('error');
        });
        $('#jsendit').click(function() {
            var form_full_fill = true;
            var pattern = /^([a-zA-Z0-9_.+-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;

            $('#jcontact_errors').html('');
            $('#jcontact_form input, #jcontact_form textarea').removeClass('error');

            if ($('#jcontact_form input[name="name"]').val().length < 2) {
                form_full_fill = false;
                $('#jcontact_form input[name="name"]').addClass('error');
            }
            if ($('#jcontact_form input[name="email"]').val().length > 1 && !pattern.test($('#jcontact_form input[name="email"]').val())) {
                form_full_fill = false;
                $('#jcontact_form input[name="email"]').addClass('error');
            }
            if ($('#jcontact_form textarea[name="message"]').val().length < 2) {
                form_full_fill = false;
                $('#jcontact_form textarea[name="message"]').addClass('error');
            }
            if (form_full_fill) {
                var contactResult = document.getElementById('contact_result');
                $('#jcontact_form input, #jcontact_form textarea').attr("readonly", "readonly");
                $('#jsendit').html('Enviando...').attr("disabled", "disabled");
                $.ajax({
                        url: '/contacto.php',
                        type: 'POST',
                        data: $('#jcontact_form').serialize()
                    })
                    .done(function() {
                        contactResult.classList.add('alert-success');
                        contactResult.innerHTML='👌🏾 Mensaje enviado';
                        document.getElementById('jnospam').remove();
                        document.getElementById('jcontact_form').innerHTML='';
                    })
                    .fail(function() {
                        contactResult.classList.add('alert-danger');
                        contactResult.innerHTML='🤦 Fallo enviando mensaje';
                        $('#jcontact_form input, #jcontact_form textarea').removeAttr("readonly");
                        $('#jcontact_errors').html('🤦 No se pudo enviar el mensaje, revisa el contenido');
                        $('#jsendit').html('Enviar').removeAttr("disabled");
                    });
                    $('html,body').animate({
                        scrollTop: $("article.detail-post").offset().top
                    }, 'slow');
            } else {
                form_full_fill = true;
                $('#jcontact_errors').html('Hay horrores en el formulario, corrígelos antes de enviar');
            }
        });
    }
})();
